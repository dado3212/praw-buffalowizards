## IMPORT STATEMENTS ##
import praw
from time import sleep
import requests
import glob
import os.path
import shutil

### FUNCTIONS ###

def get_subject(title):
	rob_words = ['rob','rurikar','bruce','willakers','old man', 'rawb']
	deadbones_words = ['deadbones']
	spyd_words = ['spyd','spydster']
	justin_words = ['hcjustin', 'justin']
	roamin_words = ['roamin','paladin']
	vankz_words = ['vankz']
	coestar_words = ['coe','coestar']
	group_words = ['buffalo wizards']

	subject = ['unknown','Unknown']

	if any(x in title for x in rob_words):
		subject = ['rob','Rob']
	if any(x in title for x in deadbones_words):
		subject = ['deadbones','Deadbones']
	if any(x in title for x in spyd_words):
		subject = ['spyd','Spyd']
	if any(x in title for x in justin_words):
		subject = ['justin','Justin']
	if any(x in title for x in roamin_words):
		subject = ['roamin','Roamin']
	if any(x in title for x in vankz_words):
		subject = ['vankz','Vankz']
	if any(x in title for x in coestar_words):
		subject = ['coestar','Coestar']
	if any(x in title for x in group_words):
		subject = ['bw','Buffalo Wizard Group']

	return subject

def get_target(url,subject):
	url_extension = '.'+url.split('.')[-1]
	if 'com' in url_extension:
		url = 'http://i.' + url.split('//')[1] + '.jpg'
		url_extension = '.'+url.split('.')[-1]

	save_directory = 'C:\Users\Alex Beals\Documents\File Holder - Rainmeter\Reddit\Buffalo Wizards\\fan art\\'+subject[0]+'\\'

	img_count = str(len(glob.glob(save_directory+'*'))+1)

	target_file = save_directory+subject[0]+img_count+url_extension

	return [target_file,url]

def new(link):
	for comment in praw.helpers.flatten_tree(link.comments):
		if comment.author == 'buffalo_bot':
			return False
	return True

## DEFINITION OF VARIABLES ##
USERNAME = 'buffalo_bot'
PASSWORD = 'access'
USERAGENT = 'Fan Art Amalgamator v1.0 by /u/dado3212'

## LOGIN ##
r = praw.Reddit(USERAGENT)
r.login(USERNAME,PASSWORD)

## LOOP CODE ##
sub = r.get_subreddit('buffalowizards')

running = True
while running:

	links = sub.get_new(limit=10) #get 10 newest posts

	for item in links:

		title = item.title.lower() #gets the submission title
		flair = item.link_flair_css_class

		## SETS FLAIR ##
		if 'murder' in title and 'garry' in title and flair is None:
			sub.set_flair(item,'Murder','murder')
		if 'tabletop' in title and flair is None:
			sub.set_flair(item,'Tabletop','tabletop')

		if flair is not None and 'fan-art' in flair and new(item):

			subject = get_subject(title)

			# URL OBTAIN #
			
			target_file = get_target(item.url,subject)

			# SAVE FILE #

			f = open(target_file[0],'wb')
			f.write(requests.get(target_file[1]).content)
			f.close()

			sleep(10)

			# MOVE TO DROPBOX #

			shutil.copy2(target_file[0],'C:\Users\Alex Beals\Dropbox\Buffalo Wizards - Fan Art')

			# COMMENT ADDED (to break loop) #

			item.add_comment('This image has been automatically uploaded to the Dropbox of fan-art, located by navigating the wiki, or through [**this link**](https://www.dropbox.com/sh/jfubzkonvtd821o/AAA9TV2Mverd2H9pqGBcc6EIa).')
			print 'Image of ' + subject[1] + ' added.'
	try:
		sleep(600) #wait for 10 minutes

	except KeyboardInterrupt:
		running = FalseAlex Beals
